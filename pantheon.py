#! /usr/bin/python3

import os.path
import os
import enum
import sys

GKSUDO = 1
KDESU = 2

def main():
    
    result = checkDependencies()
    if result == GKSUDO:
        run(GKSUDO)
    elif result == KDESU:
        run(KDESU)

    print("Application Exits...")
def run(runningMode):
    """ This will run the command to install pantheon via pacman """
    
    pacmanString = "cerbere-bzr gala-bzr wingpanel-bzr slingshot-launcher-bzr audience-bzr contractor-bzr eidete-bzr elementary-icon-theme-bzr elementary-scan-bzr elementary-wallpapers-bzr gtk-theme-elementary-bzr footnote-bzr geary indicator-session maya-calendar-bzr midori-granite-bzr noise-bzr pantheon-backgrounds-bzr pantheon-calculator-bzr pantheon-default-settings-bzr pantheon-files-bzr pantheon-print-bzr pantheon-terminal-bzr plank-theme-pantheon-bzr scratch-text-editor-bzr snap-photobooth-bzr switchboard-bzr ttf-opensans ttf-dejavu ttf-droid ttf-freefont ttf-liberation pantheon-session-bzr gnome-tweak-tool file-roller switchboard-plug-about-bzr switchboard-plug-applications-bzr switchboard-plug-datetime-bzr switchboard-plug-default-applications-bzr switchboard-plug-display-bzr switchboard-plug-elementary-tweaks-bzr switchboard-plug-keyboard-bzr switchboard-plug-locale-bzr switchboard-plug-notifications-bzr switchboard-plug-pantheon-shell-bzr switchboard-plug-power-bzr switchboard-plug-security-privacy-bzr switchboard-plug-startup-applications-bzr granite-bzr gsignond-extension-pantheon-bzr gsignond-plugin-oauth-git gsignond-plugin-sasl-git gsignond ido indicator-datetime indicator-sound libappindicator libgsignon-glib libindicate libindicator pantheon-default-settings-bzr pantheon-dock-bzr pantheon-files-plugin-dropbox-bzr pantheon-photos-bzr scour dconf-editor"
    
    if runningMode == GKSUDO:
        os.system("gksudo -m 'Installing Pantheon Desktop components..." "pacman -S --needed --noconfirm '" + pacmanString)
    
    elif runningMode == KDESU:
        os.system("kdesu -nc 'pacman -S --needed --noconfirm '" + pacmanString)
    
    
def checkDependencies():
    """ This will check dependencies for gksudo kdesu"""
    
    #gksudo check
    if os.path.exists("/usr/bin/gksudo") and os.path.isfile("/usr/bin/gksudo"):
        print("GKSUDO found")
        return GKSUDO
    else:
        print("WARNING: GKSUDO not found")
        
    #kdesu check
    if os.path.exists("/usr/bin/kdesu") and os.path.isfile("/usr/bin/kdesu"):
        print("KDESU found")
        return KDESU
    else:
        print("WARNING: KDESU not found")
        
        #Failed if none above
        print("Failed to initiate Graphical Sudo")
        return -1
    
if __name__=="__main__":
    main()