# PantheonPythonInstaller
<br>
An installer made with Python to install Pantheon on Arch Linux.<br>
You can use either Python installer (pantheon.py) or Bash installer (pantheon.sh)<br>
Bash installer is recommended, since Python installer does not output anything.
<br>
Tested on:<br>
##Desktop: KDE Plasma 5.3.1<br>
  Distro: Arch Linux (Manjaro KDE 8.13 RC1)<br>
  Python: python3<br>
  Graphical Sudo: kdesu<br>
  DateTime: 2015/05/30, 23:27<br>
  Tested by: Katsuyori<br>